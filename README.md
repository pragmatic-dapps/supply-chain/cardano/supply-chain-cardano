# supply-chain-cardano

## Prerequirements

This project is based upon Cardano project and has only one purpose - run simple supply chain case on Cardano network.
To successfully run the test part Cardano Mallet tool should be installed and Cardano accounts have to be created. In
 order to have this configured, follow the steps:
```
git clone https://github.com/input-output-hk/mallet
```
```
cd mallet
```
```
npm install
```

## Setup project
```
npm install
```

## Run tests

1. Create accounts in Cardano test network
```
node createAccounts.js
```

2. Add gas to created accounts
```
node addFunds.js
```

3. Compile smart contracts
```
truffle compile
```

4. Deploy smart contracts
```
node deploy.js
```

5. Run tests

TODO




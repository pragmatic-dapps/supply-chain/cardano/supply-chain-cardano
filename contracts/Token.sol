pragma solidity ^0.4.24;

import "openzeppelin-solidity/contracts/math/SafeMath.sol";
import "openzeppelin-solidity/contracts/token/ERC20/ERC20.sol";
import "openzeppelin-solidity/contracts/ownership/Ownable.sol";

// Crowd sale token contract
// (c) by Pragmatic DLT
contract Token is ERC20, Ownable {
    using SafeMath for uint;

    string public name;
    string public symbol;
    uint public decimals;
    uint public totalSupply;
    uint public startDate;
    uint public bonusEnds;
    uint public endDate;

    // Amount of tokens per 1 ETH
    uint public exchangeRate;
    uint public bonusExchangeRate;

    mapping(address => uint) balances;
    mapping(address => mapping(address => uint)) approved;

    constructor() public {
        symbol = "TST";
        name = "Test ERC20 token";
        decimals = 18;
        bonusEnds = now + 1 weeks;
        endDate = now + 7 weeks;
        exchangeRate = 1000;
        bonusExchangeRate = 1200;

        balances[0x2D11224f7F2A1917EDfF5d4763D08B877eA180EF] = 1000 * 10 ** uint(decimals);
        balances[0x605BfAd8Ad62B504fc4724a0958D9c6A57Bd56AE] = 1000 * 10 ** uint(decimals);
        balances[0x7d185E5b5b2be54668ce3Ba75Ffcc8EF025dde85] = 1000 * 10 ** uint(decimals);
        balances[0x67a98D167D1452F7ce02c46B02c906Fe6a0C87bA] = 1000 * 10 ** uint(decimals);
        balances[0x476642850bC9e01F4Bc84795D90b298B2465455b] = 1000 * 10 ** uint(decimals);
        balances[0x86DA497d0791dDcc603174aa311808C0ECEea9F7] = 1000 * 10 ** uint(decimals);
        balances[0x13e85fD3026A7676b9a22D7b20028E527f9d4619] = 1000 * 10 ** uint(decimals);
        balances[0x2402607CEe1055c34b618a138065015504a1CBAb] = 1000 * 10 ** uint(decimals);
        balances[0x0684cfe49091aE373c53c7440745259eD2921eA3] = 1000 * 10 ** uint(decimals);
        balances[0xb5B78eBA82199740fE14F9ed9a382F51E055Ec75] = 1000 * 10 ** uint(decimals);
        balances[0xfb7Aee67Ae789E2C56A992E48e4B6a5409577d50] = 1000 * 10 ** uint(decimals);

    }

    function totalSupply() public view returns (uint) {
        return totalSupply - balances[address(0)];
    }

    function balanceOf(address _tokenOwner) public view returns (uint) {
        return balances[_tokenOwner];
    }

    // Returns the amount which _spender is still allowed to withdraw from _owner
    function allowance(address _owner, address _spender) public view returns (uint) {
        return approved[_owner][_spender];
    }

    // Transfers _value amount of tokens to address _to, and fires the Transfer event.
    // If the _from account balance does not have enough tokens to spend - revert.
    // Transfers of 0 values are treated as normal transfers and the Transfer event is fired.
    function transfer(address _to, uint _value) public returns (bool) {
        require(balances[msg.sender] >= _value);
        balances[msg.sender] = balances[msg.sender].sub(_value);
        balances[_to] = balances[_to].add(_value);

        emit Transfer(msg.sender, _to, _value);
        return true;
    }

    // Transfers _value amount of tokens from address _from to address _to and fires the Transfer event.
    // Transfers of 0 values are treated as normal transfers and the Transfer event is fired.
    // The calling account must already have sufficient tokens approved
    function transferFrom(address _from, address _to, uint _value) public returns (bool) {
        require(balances[_from] >= _value);
        require(approved[_from][msg.sender] >= _value);

        balances[_from] = balances[_from].sub(_value);
        approved[_from][msg.sender] = approved[_from][msg.sender].sub(_value);
        balances[_to] = balances[_to].add(_value);

        emit Transfer(_from, _to, _value);
        return true;
    }

    // Allows _spender to withdraw multiple times, up to the _value amount.
    // If this function is called again it overwrites the current allowance with _value.
    function approve(address _spender, uint _value) public returns (bool) {
        approved[msg.sender][_spender] = _value;

        emit Approval(msg.sender, _spender, _value);
        return true;
    }

    function() public payable {
        require(now >= startDate && now <= endDate);
        require(msg.value >= 1 finney);

        uint tokens;

        if (now <= bonusEnds) {
            tokens = msg.value.mul(exchangeRate);
        } else {
            tokens = msg.value.mul(bonusExchangeRate);
        }

        balances[msg.sender] = balances[msg.sender].add(tokens);
        totalSupply = totalSupply.add(tokens);

        emit Transfer(address(0), msg.sender, tokens);
        owner.transfer(msg.value);
    }

    // Token owner can approve for _spender to 'transferFrom' tokens from the token owner's account.
    // The `spender` contract function `receiveApproval` is then executed
    function approveAndCall(address _spender, uint _value, bytes _data) public returns (bool) {
        approved[msg.sender][_spender] = _value;

        emit Approval(msg.sender, _spender, _value);
        ApproveAndCallFallBack(_spender).receiveApproval(msg.sender, _value, this, _data);
        return true;
    }

    // Owner can transfer out any accidentally sent ERC20 tokens
    function transferAnyERC20Token(address _tokenAddress, uint _value) public returns (bool) {
        return ERC20(_tokenAddress).transfer(owner, _value);
    }
}

// Contract function to receive approval and execute function in one call
contract ApproveAndCallFallBack {
    function receiveApproval(address _from, uint256 _value, address _tokenAddress, bytes _data) public;
}

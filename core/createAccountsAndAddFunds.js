const fs = require("fs");
const path = require("path");

const Web3 = require('web3');
const crypto = require('crypto');
const request = require('request-promise-native');

const TARGET_ACCOUNT_BALANCE = 10000000000000000;
const FAUCET_INTERVAL = 60000;
const WAIT_BETWEEN_CALLS = 60000;

const providerUrl = 'https://kevm-testnet.iohkdev.io:8546';
const web3 = new Web3(providerUrl);

const createAccounts = async () => {
  const brands = {};
  for (let i = 1; i < 11; i++) {
    console.log(`Creating account #${i}`);

    const rand = crypto.randomBytes(32).toString('hex');
    const account = web3.eth.accounts.privateKeyToAccount("0x" + rand + "");
    await web3.eth.accounts.wallet.add(account);
    await fund(account);
    brands[`brand-${i}`] = {
      "address": account.address,
      "PK": account.privateKey
    };
    console.log(`Account #${i} has been created and funded`);
    console.log('------------------------------');
  }
  writeData(path.join(__dirname, "../data", 'testData.json'), JSON.stringify(brands));
};

const createMainAccount = async () => {
  console.log('Creating main account...');
  const rand = crypto.randomBytes(32).toString('hex');
  const account = web3.eth.accounts.privateKeyToAccount("0x" + rand + "");
  await web3.eth.accounts.wallet.add(account);
  await fund(account);

  writeData(path.join(__dirname, "../data", 'authData.json'), JSON.stringify({
    "supplychain" : {
      "address" : account.address,
      "PK" : account.privateKey
    }
  }));
  console.log('Main account');
  console.log('------------------------------');
};

const fund = async (account) => {
  let balance = parseInt(await web3.eth.getBalance(account.address), 10);
  while (balance <= TARGET_ACCOUNT_BALANCE) {
    await new Promise(async (res,rej) => {
      console.log('Requesting more test tokens from faucet (waiting ' + FAUCET_INTERVAL / 1000 + ' seconds)')
      const url = "https://kevm-testnet.iohkdev.io:8099/faucet?address=" + account.address;
      try {
        await request.post(url)
      } catch (err) {
        console.log(err.message);
        process.exit();
      }
      let funded = false;
      const interval = setInterval(async () => {
        const newbalance = parseInt(await web3.eth.getBalance(account.address), 10);
        if (newbalance > balance) {
          res();
          clearInterval(interval);
        }
      }, FAUCET_INTERVAL)
    });
    balance = parseInt(await web3.eth.getBalance(account.address), 10);
    console.log('Account balance = ' + balance);
    await sleep(WAIT_BETWEEN_CALLS)
  }
};

const writeData = (filePath, data) => {
  fs.unlinkSync(filePath);
  fs.writeFileSync(filePath, data);
};

function sleep(ms) {
  return new Promise(resolve => setTimeout(resolve, ms));
}

const create = async () => {
  await createMainAccount();
  await createAccounts();
};

create().then(() => console.log("Finished."));
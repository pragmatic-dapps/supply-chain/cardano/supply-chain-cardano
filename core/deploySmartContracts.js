const TokenContract = require('./../build/contracts/Token.json');
const OrderManagerContract = require('./../build/contracts/OrderManager.json');

const authData = require("./../data/authData.json");
const fs = require("fs");
const path = require("path");
const Web3 = require('web3');

const providerUrl = 'https://kevm-testnet.iohkdev.io:8546';
const web3 = new Web3(providerUrl);

const deployERC20 = async () => {
    console.log("ERC20 contract is being deployed ...");
    await web3.eth.accounts.wallet.add(authData.supplychain.PK);
    const tx = { data: TokenContract.bytecode };

    const instance = new web3.eth.Contract(TokenContract.abi);
    const erc20Contract = await instance.deploy(tx).send({
        from: authData.supplychain.address,
        gas: 5000000,
        gasPrice: 5000000000
    });
    console.log("ERC20 contract has been deployed. Address: " + erc20Contract.options.address);
    return erc20Contract.options.address;
};

const deployOrderManager = async (erc20Address) => {
    console.log("OrderManager contract is being deployed ...");
    const tx =  {
        data: OrderManagerContract.bytecode,
        arguments: [erc20Address]
    };
    const instance = new web3.eth.Contract(OrderManagerContract.abi);
    const omContract = await instance.deploy(tx).send({
        from: authData.supplychain.address,
        gas: 5000000,
        gasPrice: 5000000000
      });
    console.log("OrderManager contract has been deployed. Address: " + omContract.options.address);
    return omContract.options.address;
};

const deploy = async () => {
  await web3.eth.accounts.wallet.add(authData.supplychain.PK);
  const erc20Address = await deployERC20();
  const omAddress = await deployOrderManager(erc20Address);
  const pathToFile = path.join(__dirname, "../data", 'contracts-info.json');
  await fs.unlinkSync(pathToFile);
  fs.writeFileSync(pathToFile, JSON.stringify({erc20Address: erc20Address, omAddress: omAddress}));
};

deploy().then(() => console.log("Finished."));

const token = require('./../build/contracts/Token.json');
const orderManager = require('./../build/contracts/OrderManager.json');
const contractsInfo = require('../build/contracts-info.json');
const testData = require('./../data/testData.json');
const authData = require('../data/authData.json');

const Mallet = require('../../mallet/lib/mallet.js');
const mallet = new Mallet('kevm', '/tmp/basic-kevm-test');

const addAccount = async () => {

  return await mallet.importPrivateKey();
};

const getTotalSupply = async () => {
    mallet.selectAccount(authData.supplychain.address);

    return await mallet.iele.contractCall({
        to: contractsInfo.erc20Address,
        gas: 1000000,
        func: 'totalSupply()', args: []
    });
};

const getParticipant = async (address) => {
    mallet.selectAccount(authData.supplychain.address);

    return await mallet.iele.contractCall({
        to: contractsInfo.erc20Address,
        gas: 1000000,
        func: `getParticipant(${address})`,
        args: []
    });
};

const registerParticipant = async (address, name) => {
    mallet.selectAccount(authData.supplychain.address);

    return await mallet.iele.contractCall({
        to: contractsInfo.omAddress,
        gas: 1000000,
        func: `createBrand(${address},${name})`,
        args: []
    });
};

// getTotalSupply().then(supply => console.log(supply));
//getParticipant(testData['brand-1'].address).then(p => console.log(p));
//  registerParticipant(testData['brand-1'].address, 'Jack Daniels 2').then(() => getParticipant(testData['brand-1'].address).then(p => console.log(p)));
 registerParticipant(testData['brand-1'].address, 'Jack Daniels 3').then(p => console.log(p));

//console.log(mallet.getReceipt('0x5999f8e426c7c69425081f57f4b8eabce0e8a8fc943324de5c8596ec8f2fb148'))

// 0x362749e4c1fde2d802c698d9049d565ca314705d65a0d14946549b21c137b4ea

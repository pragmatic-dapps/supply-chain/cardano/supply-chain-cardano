const token = require('./../build/contracts/Token.json');
const orderManager = require('./../build/contracts/OrderManager.json');
const contractsInfo = require('../data/contracts-info.json');
const testData = require('./../data/testData.json');
const authData = require('../data/authData.json');
const crypto = require('crypto');
const request = require('request-promise-native');

const Web3 = require('web3');
const providerUrl = 'https://kevm-testnet.iohkdev.io:8546';
const web3 = new Web3(providerUrl);


const orderManagerContract = new web3.eth.Contract(orderManager.abi, contractsInfo.omAddress);
const erc20Contract = new web3.eth.Contract(token.abi, contractsInfo.erc20Address);

const getParticipant = async (address) => {
    return await orderManagerContract.methods.getParticipant(address).call();
};

const registerParticipant = async (address, name) => {
    await web3.eth.accounts.wallet.add(authData.supplychain.PK);
    const estimatedGas = await orderManagerContract.methods.createBrand(address, name).estimateGas();

    const sendResult = await orderManagerContract.methods.createBrand(address, name).send({
        from: authData.supplychain.address,
        gas: estimatedGas,
        gasPrice: 5000000000
      });

    return JSON.stringify(sendResult, null, 4);
};

const approve = async () => {
  await web3.eth.accounts.wallet.add(authData.supplychain.PK);
  await web3.eth.accounts.wallet.add(testData['brand-10'].PK);
  const estimatedGas = await erc20Contract.methods.approve(testData['brand-10'].address, 999000000000000000000).estimateGas();
  console.log(estimatedGas);
  const result = await erc20Contract.methods.approve(contractsInfo.omAddress, 999000000000000000000)
    .send({from: testData['brand-10'].address, gas: estimatedGas, gasPrice: 5000000000});

  return JSON.stringify(result, null, 4);
};

// getTotalSupply().then(supply => console.log(supply));
// details().then(supply => console.log(supply));
// read().then(supply => console.log(supply));
//allEvents().then(supply => console.log(supply));

//registerParticipant(testData['brand-10'].address, 'Jack Daniels').then(p => console.log(p));
//getParticipant(testData['brand-10'].address).then(p => console.log(p));

approve().then(p => console.log(p));
